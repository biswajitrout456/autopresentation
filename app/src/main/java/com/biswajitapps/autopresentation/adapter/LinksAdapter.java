package com.biswajitapps.autopresentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biswajitapps.autopresentation.R;
import com.biswajitapps.autopresentation.activity.WebViewActivity;
import com.biswajitapps.autopresentation.model.LinksModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Biswajit on 31/05/17.
 * SubhPay
 */

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.ViewHolder> {

    private ArrayList<LinksModel> links;
    private Context context;

    public LinksAdapter(ArrayList<LinksModel> links, Context context) {
        this.links = links;
        this.context = context;
    }

    public void setLists(ArrayList<LinksModel> links) {
        this.links = links;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.links_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinksModel link = links.get(position);
        holder.link.setText(link.getTitle());

    }

    @Override
    public int getItemCount() {
        return links.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvLink)
        TextView link;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, WebViewActivity.class)
                            .putExtra("Link", links.get(getAdapterPosition()).getLink()));
                }
            });
        }
    }
}

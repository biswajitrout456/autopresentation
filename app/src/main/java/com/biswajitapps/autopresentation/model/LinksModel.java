package com.biswajitapps.autopresentation.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Biswajit on 31/05/17.
 * SubhPay
 */

public class LinksModel {

    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private String id;
    @SerializedName("desc")
    private String description;
    @SerializedName("link")
    private String link;

    @SerializedName("Links")
    private ArrayList<LinksModel> links;

    public static LinksModel parseResponse(String array) {
        return new Gson().fromJson(array, LinksModel.class);
    }

    public ArrayList<LinksModel> getLinks(){
        return links;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

}
